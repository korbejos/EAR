# Automatizace a optimalizace odečtů spotřeby energií
Semestrální Práce do předmětu BD6B33EAR
## Hlavní funkce
- Autonomní odečty energií bez potřeby fyzického přístupu pomocí bezdrátových měřidel
- Přehled o aktuální spotřebě, o spotřebě za časový rámec atp.
- Možnost porovnání dodavatelů energií, tím pádem optimalizovat cenu za energie

## Tým
- Josef Korbel
- Lukáš Krýda
