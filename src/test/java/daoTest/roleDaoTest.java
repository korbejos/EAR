/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daoTest;

import cz.korbelkryda.earsemestralka.dao.RoleDao;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Jsf
 */
public class roleDaoTest extends BaseDaoTestRunner {
    
    @Autowired
    RoleDao roleDao;
    
    @Test
    public void testAdminAccountExistency(){
        assertNotNull("Admin Account was not found in roleDao", roleDao.findById(0));
    }
    
}
