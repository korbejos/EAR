/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daoTest;

import cz.korbelkryda.earsemestralka.model.Flat;
import cz.korbelkryda.earsemestralka.dao.FlatDao;
import cz.korbelkryda.earsemestralka.dao.UserDao;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Jsf
 */
public class flatDaoTest extends BaseDaoTestRunner { 
    
    @Autowired private FlatDao flatDao;    
    
    @Test
    public void checkInsert() {
        int before = flatDao.findAll().size();
        
        Flat flat = new Flat(2556,7); // Create Flat
        flatDao.persist(flat);
        
        int after = flatDao.findAll().size();
        assertEquals(after, before + 1);
    }
    
    @Autowired UserDao userDao;
    //@Test    
    public void checkAssignToAdmin() {
        Flat flat = new Flat(125,5);
        flat.setOwnerId(userDao.findById(0)); // Zero is AdminTestAccount
        flatDao.persist(flat);
        
        // Get Flat Owned by AdminTestAccount
        Flat result = flatDao.findByOwner(userDao.findById(0));
        
        assertNotNull(result);
        assertEquals(result.getFlatId(), flat.getFlatId());
        
    }
    
}
