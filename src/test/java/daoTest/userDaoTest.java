/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daoTest;

import cz.korbelkryda.earsemestralka.model.Users;
import cz.korbelkryda.earsemestralka.dao.UserDao;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import static org.junit.Assert.*;

/**
 *
 * @author Jsf
 */
public class userDaoTest extends BaseDaoTestRunner {
    
    @Autowired
    private UserDao userDao;
    
    @Test
    public void findByNameFindsUserByFirstNameAndLastName() {
        final Users t = new Users();
        t.setFirstname("Severus");
        t.setLastname("Snape");
        t.setEmail("severus.snape@hogwarts.co.uk");
        t.setPass("pw");
        userDao.persist(t);
        final Users result = userDao.findByName(t.getFirstname(), t.getLastname());
        assertNotNull(result);
        assertEquals("ID DOES NOT MATCH OR IT WAS NOT ADDED", t.getUserId(), result.getUserId());
    }
    
    //@Test
    public void testDataOccurence() {
        assertEquals("No or more Data", userDao.findAll().size(), 1);
    }
    
    
    
    //@Test
    public void getFirstUser() {
        assertNotNull("User with index 0 is null", userDao.findById(0));
    }
}
