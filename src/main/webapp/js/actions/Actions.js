'use strict';

import Reflux from 'reflux';

const Actions = Reflux.createActions([
    'loadUsers'
]);

export default Actions;
