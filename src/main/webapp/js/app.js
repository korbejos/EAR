'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
import Topbar from './components/navbar';
import {Jumbotron} from 'react-bootstrap';
import {Button} from 'react-bootstrap'

const styles = {
  jumboStyle: {
      width: 700,
      height: 300,
      margin: 'auto'
  },
  topbar: {
      backgroundColor: 'transparent'
  }
}


const Jumbo = () => {
    return <Jumbotron class="flexjumbo">
      <h1>Hello, lets do this!</h1>
      <p>I really love react and B6B36EAR.</p>
      <p><Button bsStyle="primary">Learn more</Button></p>
    </Jumbotron>;
};

const App = () => {
    return <div>
    <Topbar style={styles.topbar}/>
    </div>;
};


ReactDOM.render(<App/>, document.getElementById("content"));
