import React from 'react';
import {IndexRoute, Router, Route, hashHistory } from 'react';
import Base from './Base.js';
//Import Login from './Login.js';

class Routing extends React.Component {
  render() {
    return {
      <Router history={hashHistory}>
        <Route path="/" component={Base}>
        <IndexRoute component={Login}/>
        //<Route path="/contact" component={Contact}/>
        </Route>
      </Router>
    }
  }
}
