'use strict';

import Reflux from 'reflux';
import request from 'superagent';

import Actions from '../actions/Actions';

const URL = 'rest/users';

const UserStore = Reflux.createStore({
    listenables: [Actions],

    onLoadUsers: function() {
        request.get(URL).accept('json').end((err, resp) => {
            if (err) {
                console.log('Error when loading Users. Status: ' + err.status);
            } else {
                this.trigger(resp.body);
            }
        });
    }
});

export default UserStore;
