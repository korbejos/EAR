'use strict';

import React from 'react';
import {Navbar} from 'react-bootstrap'
import {Nav} from 'react-bootstrap';
import {NavItem} from 'react-bootstrap';
import {NavDropdown} from 'react-bootstrap';
import {MenuItem} from 'react-bootstrap';


const Topbar = () => {
    return <Navbar inverse>
    <Navbar.Header>
      <Navbar.Brand>
        <a href="#">EnergyWarden</a>
      </Navbar.Brand>
    </Navbar.Header>
    <Nav pullRight>
        <NavItem eventKey={1} href="#">Login</NavItem>
        <NavItem eventKey={2} href="#">Contact</NavItem>
      </Nav>
  </Navbar>
};

export default Topbar;
