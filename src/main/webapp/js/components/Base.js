import React from 'react';
import {Topbar} from './components/navbar';

class Base extends React.Component {

    render() {
      return {
        <div>
            <Topbar/>
        </div>
      }
    }
}

export default Base;
