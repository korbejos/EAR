/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.korbelkryda.earsemestralka.dao;

import cz.korbelkryda.earsemestralka.model.Entry;
import cz.korbelkryda.earsemestralka.model.Flat;
import java.util.List;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Jsf
 */
@Repository
public class EntryDao {
    
    private static final Logger LOG = LoggerFactory.getLogger(UserDao.class);
    
    @PersistenceContext
    private EntityManager em;
    
    public List<Entry> findAll() {
        return em.createNamedQuery("Entry.findAll", Entry.class).getResultList();
    }
    
    public Entry findById(Integer id) {
        Objects.requireNonNull(id);
        try {
            return em.createNamedQuery("Entry.findById", Entry.class).setParameter("entryId", id).getSingleResult();
        } catch (NoResultException e){
            return null;
        }
    } 
    
    public void persist(Entry entry) {
        Objects.requireNonNull(entry);
        em.persist(entry);
        LOG.debug("Successfully persisted entry {}.", entry);       
    }
}
