/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.korbelkryda.earsemestralka.dao;

import cz.korbelkryda.earsemestralka.model.House;
import java.util.List;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Jsf
 */
public class HouseDao {
    
    private static final Logger LOG = LoggerFactory.getLogger(FlatDao.class);
    
    @PersistenceContext
    private EntityManager em;
    
    public List<House> findAll() {
        return em.createNamedQuery("House.findAll", House.class).getResultList();
    }
    
    public House findById(Integer id) {
        Objects.requireNonNull(id);
        try {
            return em.createNamedQuery("House.findById", House.class).setParameter("flatId", id).getSingleResult();
        } catch (NoResultException e){
            return null;
        }
    } 
    
    public void persist(House house) {
        Objects.requireNonNull(house);
        em.persist(house);
        LOG.debug("Successfully persisted house {}.", house);       
    }    
    
}
