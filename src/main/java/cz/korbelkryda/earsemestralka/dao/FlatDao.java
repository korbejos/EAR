/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.korbelkryda.earsemestralka.dao;

import cz.korbelkryda.earsemestralka.model.Flat;
import cz.korbelkryda.earsemestralka.model.Users;
import java.util.List;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Josef Korbel
 */
@Repository
public class FlatDao {
    
    private static final Logger LOG = LoggerFactory.getLogger(FlatDao.class);
    
    @PersistenceContext
    private EntityManager em;
    
    public List<Flat> findAll() {
        return em.createNamedQuery("Flat.findAll", Flat.class).getResultList();
    }
    
    public Flat findById(Integer id) {
        Objects.requireNonNull(id);
        try {
            return em.createNamedQuery("Flat.findById", Flat.class).setParameter("flatId", id).getSingleResult();
        } catch (NoResultException e){
            return null;
        }
    } 
    
    /**
     * Show Flat by OwnerId
     * @param owner Id of Owner (@Users)
     * @return Flat object
     */
    public Flat findByOwner(Users owner) {
        Objects.requireNonNull(owner);
        try {
            return em.createNamedQuery("Flat.findByOwner", Flat.class).setParameter("ownerId", owner).getSingleResult();
        } catch (NoResultException e){
            return null;
        }
    }

    public void persist(Flat flat) {
        Objects.requireNonNull(flat);
        em.persist(flat);
        LOG.debug("Successfully persisted flat {}.", flat);       
    }    
    
    
}
