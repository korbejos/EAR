/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.korbelkryda.earsemestralka.dao;

import cz.korbelkryda.earsemestralka.model.Roles;
import cz.korbelkryda.earsemestralka.model.Users;
import java.util.List;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Jsf
 */
@Repository
public class RoleDao {
    
    private static final Logger LOG = LoggerFactory.getLogger(RoleDao.class);
    
    @PersistenceContext
    private EntityManager em;   
    
    
    public List<Roles> findAll() {
        return em.createNamedQuery("Roles.findAll", Roles.class).getResultList();
    }
    
    public Roles findById(Integer id) {
        Objects.requireNonNull(id);
        try {
            return em.createNamedQuery("Roles.findById", Roles.class).setParameter("roleId", id).getSingleResult();
        } catch (NoResultException e){
            return null;
        }
    }
    
    public void persist(Roles role) {
        Objects.requireNonNull(role);
        em.persist(role);
        LOG.debug("Successfully persisted role {}.", role); 
    }
}
