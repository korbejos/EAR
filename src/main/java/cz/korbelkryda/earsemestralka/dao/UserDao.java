/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.korbelkryda.earsemestralka.dao;

import cz.korbelkryda.earsemestralka.model.Users;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Jsf
 */
@Repository
public class UserDao {
    
    private static final Logger LOG = LoggerFactory.getLogger(UserDao.class);
    
    @PersistenceContext
    private EntityManager em;   
    
    
    public List<Users> findAll() {
        return em.createNamedQuery("Users.findAll", Users.class).getResultList();
    }
    
    public Users findById(Integer id) {
        Objects.requireNonNull(id);
        try {
            return em.createNamedQuery("Users.findById", Users.class).setParameter("userId", id).getSingleResult();
        } catch (NoResultException e){
            return null;
        }
    }
    
    public Users findByName(String firstName, String lastName) {
        Objects.requireNonNull(firstName);
        Objects.requireNonNull(lastName);
        try {
            return em.createNamedQuery("Users.findByName", Users.class).setParameter("firstName", firstName)
                     .setParameter("lastName", lastName).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public void persist(Users user) {
        Objects.requireNonNull(user);
        em.persist(user);
        LOG.debug("Successfully persisted user {}.", user);       
    }
}
