/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.korbelkryda.earsemestralka.service;

import cz.korbelkryda.earsemestralka.model.Users;
import cz.korbelkryda.earsemestralka.dao.UserDao;
import java.util.Collection;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Jsf
 */
@Service
public class UserService {
    
    @Autowired
    UserDao userDao;
    
    @Transactional(readOnly = true)
    public List<Users> findAll() {
        return userDao.findAll();
    }

    @Transactional(readOnly = true)
    public boolean exists(String firstName, String lastName) {
        return userDao.findByName(firstName, lastName) != null;
    }
    
    @Transactional
    public void persist(Users user) {
        userDao.persist(user);
    }
    
}
