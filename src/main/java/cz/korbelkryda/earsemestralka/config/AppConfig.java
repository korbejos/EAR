/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.korbelkryda.earsemestralka.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.EnableMBeanExport;
import org.springframework.context.annotation.Import;

/**
 * Root configuration file of our project - it sets up basic Spring configuration and imports additional configuration files.
 * <p>
 * It is good to separate configuration of different components of the application, because they can then be configured
 * independently for example in tests.
 */
// This annotation is required when services without separate interfaces are used. It causes cglib-based proxies of
// the services to be used - see http://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/context/annotation/EnableAspectJAutoProxy.html#proxyTargetClass--
@EnableAspectJAutoProxy(proxyTargetClass = true)
@Configuration  // This class is a Spring configuration
@Import({WebAppConfig.class, PersistenceConfig.class, ServiceConfig.class}) // Import additional configuration classes
public class AppConfig {
}
