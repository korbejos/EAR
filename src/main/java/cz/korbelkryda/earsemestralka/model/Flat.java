/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.korbelkryda.earsemestralka.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jsf
 */
@Entity
@Table(name = "flat")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Flat.findAll", query = "SELECT f FROM Flat f"),
    @NamedQuery(name = "Flat.findById", query = "SELECT f FROM Flat f WHERE f.flatId = :flatId"),
    @NamedQuery(name = "Flat.findByOwner", query = "SELECT f FROM Flat f WHERE f.ownerId = :ownerId")})

public class Flat implements Serializable {
    
    @Id
    @Basic(optional = false)
    @GeneratedValue
    @Column(name = "flat_id")
    private Integer flatId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "space")
    private float space;
    
    @JoinColumn(name = "entry_id", referencedColumnName = "entry_id")
    @ManyToOne
    private Entry entryId;
    
    @JoinColumn(name = "unit_id", referencedColumnName = "unit_id")
    @ManyToOne
    private Unit unitId;
    
    @JoinColumn(name = "owner_id", referencedColumnName = "user_id")
    @ManyToOne
    private Users ownerId;

    public Flat() {
    }

    public Flat(Integer flatId) {
        this.flatId = flatId;
    }

    public Flat(Integer flatId, float space) {
        this.flatId = flatId;
        this.space = space;
    }
    
    public Flat(float space) {
        this.space = space;
    }

    public Integer getFlatId() {
        return flatId;
    }

    public void setFlatId(Integer flatId) {
        this.flatId = flatId;
    }

    public float getSpace() {
        return space;
    }

    public void setSpace(float space) {
        this.space = space;
    }

    public Entry getEntryId() {
        return entryId;
    }

    public void setEntryId(Entry entryId) {
        this.entryId = entryId;
    }

    public Unit getUnitId() {
        return unitId;
    }

    public void setUnitId(Unit unitId) {
        this.unitId = unitId;
    }

    public Users getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Users ownerId) {
        this.ownerId = ownerId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (flatId != null ? flatId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Flat)) {
            return false;
        }
        Flat other = (Flat) object;
        if ((this.flatId == null && other.flatId != null) || (this.flatId != null && !this.flatId.equals(other.flatId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cz.korbelkryda.earsemestralka.model.Flat[ flatId=" + flatId + " ]";
    }
    
}
