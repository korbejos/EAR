/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.korbelkryda.earsemestralka.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jsf
 */
@Entity
@Table(name = "medium")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Medium.findAll", query = "SELECT m FROM Medium m"),
    @NamedQuery(name = "Medium.findByMediumId", query = "SELECT m FROM Medium m WHERE m.mediumId = :mediumId"),
    @NamedQuery(name = "Medium.findByMediumname", query = "SELECT m FROM Medium m WHERE m.mediumname = :mediumname")})
public class Medium implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "medium_id")
    private Integer mediumId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "mediumname")
    private String mediumname;
    @OneToMany(mappedBy = "mediumId")
    private List<Unit> unitList;
    @OneToMany(mappedBy = "mediumId")
    private List<Reading> readingList;

    public Medium() {
    }

    public Medium(Integer mediumId) {
        this.mediumId = mediumId;
    }

    public Medium(Integer mediumId, String mediumname) {
        this.mediumId = mediumId;
        this.mediumname = mediumname;
    }

    public Integer getMediumId() {
        return mediumId;
    }

    public void setMediumId(Integer mediumId) {
        this.mediumId = mediumId;
    }

    public String getMediumname() {
        return mediumname;
    }

    public void setMediumname(String mediumname) {
        this.mediumname = mediumname;
    }

    @XmlTransient
    public List<Unit> getUnitList() {
        return unitList;
    }

    public void setUnitList(List<Unit> unitList) {
        this.unitList = unitList;
    }

    @XmlTransient
    public List<Reading> getReadingList() {
        return readingList;
    }

    public void setReadingList(List<Reading> readingList) {
        this.readingList = readingList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (mediumId != null ? mediumId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Medium)) {
            return false;
        }
        Medium other = (Medium) object;
        if ((this.mediumId == null && other.mediumId != null) || (this.mediumId != null && !this.mediumId.equals(other.mediumId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cz.korbelkryda.earsemestralka.model.Medium[ mediumId=" + mediumId + " ]";
    }
    
}
