/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.korbelkryda.earsemestralka.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jsf
 */
@Entity
@Table(name = "reading")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reading.findAll", query = "SELECT r FROM Reading r"),
    @NamedQuery(name = "Reading.findByReadingId", query = "SELECT r FROM Reading r WHERE r.readingId = :readingId"),
    @NamedQuery(name = "Reading.findByReaddate", query = "SELECT r FROM Reading r WHERE r.readdate = :readdate"),
    @NamedQuery(name = "Reading.findByReadvalue", query = "SELECT r FROM Reading r WHERE r.readvalue = :readvalue")})
public class Reading implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "reading_id")
    private Integer readingId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "readdate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date readdate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "readvalue")
    private float readvalue;
    @JoinColumn(name = "medium_id", referencedColumnName = "medium_id")
    @ManyToOne
    private Medium mediumId;
    @JoinColumn(name = "unit_id", referencedColumnName = "unit_id")
    @ManyToOne
    private Unit unitId;

    public Reading() {
    }

    public Reading(Integer readingId) {
        this.readingId = readingId;
    }

    public Reading(Integer readingId, Date readdate, float readvalue) {
        this.readingId = readingId;
        this.readdate = readdate;
        this.readvalue = readvalue;
    }

    public Integer getReadingId() {
        return readingId;
    }

    public void setReadingId(Integer readingId) {
        this.readingId = readingId;
    }

    public Date getReaddate() {
        return readdate;
    }

    public void setReaddate(Date readdate) {
        this.readdate = readdate;
    }

    public float getReadvalue() {
        return readvalue;
    }

    public void setReadvalue(float readvalue) {
        this.readvalue = readvalue;
    }

    public Medium getMediumId() {
        return mediumId;
    }

    public void setMediumId(Medium mediumId) {
        this.mediumId = mediumId;
    }

    public Unit getUnitId() {
        return unitId;
    }

    public void setUnitId(Unit unitId) {
        this.unitId = unitId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (readingId != null ? readingId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reading)) {
            return false;
        }
        Reading other = (Reading) object;
        if ((this.readingId == null && other.readingId != null) || (this.readingId != null && !this.readingId.equals(other.readingId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cz.korbelkryda.earsemestralka.model.Reading[ readingId=" + readingId + " ]";
    }
    
}
